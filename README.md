**Multithreaded implementation of the coloring algorithm for meshes of triangles and tetrahedra.**



*Meshes of triangles from GMSH*


*Compile serial and parallel codes with

gcc -O3 color2D.c color2D


*Run two dimensional codes with

./genmesh2D [name of input mesh triangles.msh] [output .pmsh file of postprocessed triangular mesh]

./color2D [name of preprocessed input file of triangles.pmsh] [output .cmsh file of colored triangles]



e.g.
./genmesh2D A_2D.msh A_2D.pmsh

./color2D A_2D.pmsh A_2D.cmsh



*Meshes of tetrahedra from tetgen*


*Compile serial and parallel codes with


gcc -O3 color3D.c color3D

gcc -O3 pcolor3D.c pcolor3D -pthread

*Run three dimensional codes with


./pcolor3D [name of input file name of tetrahedra (without extension)]


e.g.
./pcolor bar
