xf = 3.5;
yf = 1;
h = 0.05;
Point(1) = {0,0,0,h};
Point(2) = {xf,0,0,h};
Point(3) = {xf,yf,0,h};
Point(4) = {0,yf,0,h};

Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,1};

Line Loop(11) = {1, 2, 3, 4};
Plane Surface(12) = {11};

Physical Surface(13) = {12};
Physical Line(1) = {1, 2, 3, 4}; // coloring code needs to know which edges are boundary edges
